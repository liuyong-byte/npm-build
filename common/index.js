/**
 * 一维数组求和
 * @param {Array<number>} array- 数组
 */
export const summation = (array) => {
  return array.reduce((a, b) => a + b);
};
/**
 * 一维数组最大值
 * @param {Array<number>} array- 数组
 */
export const maximum = (array) => {
  return array.reduce((a, b) => (a > b ? a : b));
};
/**
 *  一维数组排序
 * @param {Array<number>} array- 数组
 * @param {Boolean}  judge- 判断排序 默认降序 为 true 升序
 */
export const sort = (array, judge) => {
  if (judge) {
    return array.sort((a, b) => (b - a > 0 ? -1 : 1));
  }
  return array.sort((a, b) => (b - a > 0 ? 1 : -1));
};

/**
 *  一维数组排序
 * @param {Array<{}>} array- 数组
 * @param {number} sign- 数组指定排序的属性字段 默认id
 * @param {Boolean}  judge- 判断排序 默认降序 为 true 升序
 */
export const array_obj_sort = (array, sign = 'id', judge) => {
  if (judge) {
    return array.sort((a, b) => (b[sign] - a[sign] > 0 ? -1 : 1));
  }
  return array.sort((a, b) => (b[sign] - a[sign] > 0 ? 1 : -1));
};
/**
 *  一维数组去重
 * @param {Array<number|string>} array- 数组

 */
export const array_repetition = (array) => {
  return [...new Set(array)];
};
/**
 * 字符串计数
 * @param {string} string- 字符串

 */
export const array_str_total = (str) => {
  const obj = {};
  const arr = str.split('');
  arr.map((res) => {
    if (obj[res]) {
      obj[res] = obj[res] + 1;
    } else {
      obj[res] = 1;
    }
  });
  return obj;
};
/**
 * 字符串计数
 * @param {array} array- 字符串

 */
export const array_random = (array) => {
  const list = array;
  list.sort(() => {
    return Math.random() - 0.5;
  });
  return list;
};
