import path from 'path';
export default [
  {
    mode: 'production', // '@babel/polyfill', 'babel-polyfill', '@babel/preset-env', @babel/transform-runtime
    entry: ['./src/index.js'], // 允许您使用最新的 JavaScript，而无需微观管理目标环境所需的语法转换（以及可选的浏览器 polyfill）。这既让你的生活更轻松，又让JavaScript捆绑包更小
    output: {
      filename: 'index.window1.js', // 通过热加载输出script文件挂载在目录与index.HTML一样
      path: path.resolve(process.cwd(), 'text')
      // library: 'name'
      // library: {
      //   name: 'umd',
      //   type: 'window'
      // }
    },

    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                presets: [
                  [
                    '@babel/preset-env',
                    {
                      targets: {
                        edge: '17',
                        firefox: '60',
                        chrome: '67',
                        safari: '11.1',
                        ie: '11'
                      },
                      useBuiltIns: 'usage' // 开启选项 专义 @babel/preset-env 和 browserslist 配置来决定项目需要的 polyfill。
                    }
                  ]
                ],
                plugins: ['@babel/transform-runtime']
              }
            }
            //   'eslint-loader'
          ],
          include: [
            path.resolve(process.cwd(), 'src'),
            path.resolve(process.cwd(), 'bin'),
            path.resolve(process.cwd(), 'es'),
            path.resolve(process.cwd(), 'text')
          ],
          // // include: [resolve('src'), resolve('test'), resolve('node_modules/webpack-dev-server/client')],
          exclude: /node_modules/
        }
      ]
    }
  }
  // {
  //   mode: 'production', // '@babel/polyfill', 'babel-polyfill', '@babel/preset-env', @babel/transform-runtime
  //   entry: ['./text/index.js'], // 允许您使用最新的 JavaScript，而无需微观管理目标环境所需的语法转换（以及可选的浏览器 polyfill）。这既让你的生活更轻松，又让JavaScript捆绑包更小
  //   output: {
  //     filename: 'index.window.js', // 通过热加载输出script文件挂载在目录与index.HTML一样
  //     path: path.resolve(process.cwd(), 'text'),
  //     // library: 'name'
  //     library: {
  //       name: 'umd',
  //       type: 'window'
  //     }
  //   },

  //   module: {
  //     rules: [
  //       {
  //         test: /\.(js|jsx)$/,
  //         use: [
  //           {
  //             loader: 'babel-loader',
  //             options: {
  //               presets: [
  //                 [
  //                   '@babel/preset-env',
  //                   {
  //                     targets: {
  //                       edge: '17',
  //                       firefox: '60',
  //                       chrome: '67',
  //                       safari: '11.1',
  //                       ie: '11'
  //                     },
  //                     useBuiltIns: 'usage' // 开启选项 专义 @babel/preset-env 和 browserslist 配置来决定项目需要的 polyfill。
  //                   }
  //                 ]
  //               ],
  //               plugins: ['@babel/transform-runtime']
  //             }
  //           }
  //           //   'eslint-loader'
  //         ],
  //         include: [
  //           path.resolve(process.cwd(), 'src'),
  //           path.resolve(process.cwd(), 'bin'),
  //           path.resolve(process.cwd(), 'es'),
  //           path.resolve(process.cwd(), 'text')
  //         ],
  //         // // include: [resolve('src'), resolve('test'), resolve('node_modules/webpack-dev-server/client')],
  //         exclude: /node_modules/
  //       }
  //     ]
  //   }
  // },
  // {
  //   mode: 'production', // '@babel/polyfill', 'babel-polyfill', '@babel/preset-env', @babel/transform-runtime
  //   entry: ['./text/index.js'], // 允许您使用最新的 JavaScript，而无需微观管理目标环境所需的语法转换（以及可选的浏览器 polyfill）。这既让你的生活更轻松，又让JavaScript捆绑包更小
  //   experiments: {
  //     // 实验性功能的开启 写下案例的时候 因为module的支持不算完好
  //     outputModule: true
  //   },
  //   output: {
  //     filename: 'index_module.js', // 通过热加载输出script文件挂载在目录与index.HTML一样
  //     path: path.resolve(process.cwd(), 'text'),
  //     library: {
  //       type: 'module' // 开启module不需要包名称 尽量使用export default写法作为入口文件
  //     }
  //   },
  //   module: {
  //     rules: [
  //       {
  //         test: /\.(js|jsx)$/,
  //         use: [
  //           {
  //             loader: 'babel-loader',
  //             options: {
  //               presets: [
  //                 [
  //                   '@babel/preset-env',
  //                   {
  //                     targets: {
  //                       edge: '17',
  //                       firefox: '60',
  //                       chrome: '67',
  //                       safari: '11.1',
  //                       ie: '11'
  //                     },
  //                     useBuiltIns: 'usage' // 开启选项 专义 @babel/preset-env 和 browserslist 配置来决定项目需要的 polyfill。
  //                   }
  //                 ]
  //               ],
  //               plugins: ['@babel/transform-runtime']
  //             }
  //           }
  //           //   'eslint-loader'
  //         ],
  //         include: [
  //           path.resolve(process.cwd(), 'src'),
  //           path.resolve(process.cwd(), 'bin'),
  //           path.resolve(process.cwd(), 'es'),
  //           path.resolve(process.cwd(), 'text')
  //         ],
  //         // // include: [resolve('src'), resolve('test'), resolve('node_modules/webpack-dev-server/client')],
  //         exclude: /node_modules/
  //       }
  //     ]
  //   }
  // },
  // {
  //   mode: 'production', // '@babel/polyfill', 'babel-polyfill', '@babel/preset-env', @babel/transform-runtime
  //   entry: ['./text/index.js'], // 允许您使用最新的 JavaScript，而无需微观管理目标环境所需的语法转换（以及可选的浏览器 polyfill）。这既让你的生活更轻松，又让JavaScript捆绑包更小
  //   output: {
  //     filename: 'index_umd.js', // 通过热加载输出script文件挂载在目录与index.HTML一样
  //     path: path.resolve(process.cwd(), 'text'),
  //     // library: 'name'
  //     library: {
  //       name: 'umd',
  //       // type: 'window' //  umd 写法 是变成 module 与 commonjs 还有window一起
  //       type: 'umd'
  //     }
  //   },
  //   module: {
  //     rules: [
  //       {
  //         test: /\.(js|jsx)$/,
  //         use: [
  //           {
  //             loader: 'babel-loader',
  //             options: {
  //               presets: [
  //                 [
  //                   '@babel/preset-env',
  //                   {
  //                     targets: {
  //                       edge: '17',
  //                       firefox: '60',
  //                       chrome: '67',
  //                       safari: '11.1',
  //                       ie: '11'
  //                     },
  //                     useBuiltIns: 'usage' // 开启选项 专义 @babel/preset-env 和 browserslist 配置来决定项目需要的 polyfill。
  //                   }
  //                 ]
  //               ],
  //               plugins: ['@babel/transform-runtime']
  //             }
  //           }
  //           //   'eslint-loader'
  //         ],
  //         include: [
  //           path.resolve(process.cwd(), 'src'),
  //           path.resolve(process.cwd(), 'bin'),
  //           path.resolve(process.cwd(), 'es'),
  //           path.resolve(process.cwd(), 'text')
  //         ],
  //         // // include: [resolve('src'), resolve('test'), resolve('node_modules/webpack-dev-server/client')],
  //         exclude: /node_modules/
  //       }
  //     ]
  //   }
  // },
  // {
  //   mode: 'production', // '@babel/polyfill', 'babel-polyfill', '@babel/preset-env', @babel/transform-runtime
  //   entry: ['./text/index.js'], // 允许您使用最新的 JavaScript，而无需微观管理目标环境所需的语法转换（以及可选的浏览器 polyfill）。这既让你的生活更轻松，又让JavaScript捆绑包更小
  //   output: {
  //     filename: 'index_node.js', // 通过热加载输出script文件挂载在目录与index.HTML一样
  //     path: path.resolve(process.cwd(), 'text')
  //     // library: {
  //     //   name: 'umd', //包的名字用于挂载window的变量  umd 写法 是变成 module 与 commonjs 还有window一起
  //     //   type: 'commonjs'
  //     // }
  //   },
  //   // target: 'node', // 默认是web 开启node是使用require 加载chunk
  //   module: {
  //     rules: [
  //       {
  //         test: /\.(js|jsx)$/,
  //         use: [
  //           {
  //             loader: 'babel-loader',
  //             options: {
  //               presets: [
  //                 [
  //                   '@babel/preset-env',
  //                   {
  //                     targets: {
  //                       edge: '17',
  //                       firefox: '60',
  //                       chrome: '67',
  //                       safari: '11.1',
  //                       ie: '11'
  //                     },
  //                     useBuiltIns: 'usage' // 开启选项 专义 @babel/preset-env 和 browserslist 配置来决定项目需要的 polyfill。
  //                   }
  //                 ]
  //               ],
  //               plugins: ['@babel/transform-runtime']
  //             }
  //           }
  //           //   'eslint-loader'
  //         ],
  //         include: [
  //           path.resolve(process.cwd(), 'src'),
  //           path.resolve(process.cwd(), 'bin'),
  //           path.resolve(process.cwd(), 'es'),
  //           path.resolve(process.cwd(), 'text')
  //         ],
  //         // // include: [resolve('src'), resolve('test'), resolve('node_modules/webpack-dev-server/client')],
  //         exclude: /node_modules/
  //       }
  //     ]
  //   }
  // }
];
