!(function (t, n) {
  'object' == typeof exports && 'object' == typeof module
    ? (module.exports = n())
    : 'function' == typeof define && define.amd
    ? define([], n)
    : 'object' == typeof exports
    ? (exports.umd = n())
    : (t.umd = n());
})(self, function () {
  return (function () {
    var t = {
        963: function (t) {
          t.exports = function (t) {
            if ('function' != typeof t) throw TypeError(t + ' is not a function!');
            return t;
          };
        },
        7: function (t, n, r) {
          var e = r(286);
          t.exports = function (t) {
            if (!e(t)) throw TypeError(t + ' is not an object!');
            return t;
          };
        },
        32: function (t) {
          var n = {}.toString;
          t.exports = function (t) {
            return n.call(t).slice(8, -1);
          };
        },
        645: function (t) {
          var n = (t.exports = { version: '2.6.12' });
          'number' == typeof __e && (__e = n);
        },
        741: function (t, n, r) {
          var e = r(963);
          t.exports = function (t, n, r) {
            if ((e(t), void 0 === n)) return t;
            switch (r) {
              case 1:
                return function (r) {
                  return t.call(n, r);
                };
              case 2:
                return function (r, e) {
                  return t.call(n, r, e);
                };
              case 3:
                return function (r, e, o) {
                  return t.call(n, r, e, o);
                };
            }
            return function () {
              return t.apply(n, arguments);
            };
          };
        },
        57: function (t, n, r) {
          t.exports = !r(253)(function () {
            return (
              7 !=
              Object.defineProperty({}, 'a', {
                get: function () {
                  return 7;
                }
              }).a
            );
          });
        },
        457: function (t, n, r) {
          var e = r(286),
            o = r(816).document,
            i = e(o) && e(o.createElement);
          t.exports = function (t) {
            return i ? o.createElement(t) : {};
          };
        },
        985: function (t, n, r) {
          var e = r(816),
            o = r(645),
            i = r(728),
            u = r(234),
            c = r(741),
            f = function (t, n, r) {
              var a,
                p,
                s,
                l,
                v = t & f.F,
                y = t & f.G,
                d = t & f.S,
                h = t & f.P,
                x = t & f.B,
                m = y ? e : d ? e[n] || (e[n] = {}) : (e[n] || {}).prototype,
                b = y ? o : o[n] || (o[n] = {}),
                g = b.prototype || (b.prototype = {});
              for (a in (y && (r = n), r))
                (s = ((p = !v && m && void 0 !== m[a]) ? m : r)[a]),
                  (l = x && p ? c(s, e) : h && 'function' == typeof s ? c(Function.call, s) : s),
                  m && u(m, a, s, t & f.U),
                  b[a] != s && i(b, a, l),
                  h && g[a] != s && (g[a] = s);
            };
          (e.core = o), (f.F = 1), (f.G = 2), (f.S = 4), (f.P = 8), (f.B = 16), (f.W = 32), (f.U = 64), (f.R = 128), (t.exports = f);
        },
        253: function (t) {
          t.exports = function (t) {
            try {
              return !!t();
            } catch (t) {
              return !0;
            }
          };
        },
        18: function (t, n, r) {
          t.exports = r(825)('native-function-to-string', Function.toString);
        },
        816: function (t) {
          var n = (t.exports =
            'undefined' != typeof window && window.Math == Math
              ? window
              : 'undefined' != typeof self && self.Math == Math
              ? self
              : Function('return this')());
          'number' == typeof __g && (__g = n);
        },
        181: function (t) {
          var n = {}.hasOwnProperty;
          t.exports = function (t, r) {
            return n.call(t, r);
          };
        },
        728: function (t, n, r) {
          var e = r(275),
            o = r(681);
          t.exports = r(57)
            ? function (t, n, r) {
                return e.f(t, n, o(1, r));
              }
            : function (t, n, r) {
                return (t[n] = r), t;
              };
        },
        639: function (t, n, r) {
          var e = r(816).document;
          t.exports = e && e.documentElement;
        },
        734: function (t, n, r) {
          t.exports =
            !r(57) &&
            !r(253)(function () {
              return (
                7 !=
                Object.defineProperty(r(457)('div'), 'a', {
                  get: function () {
                    return 7;
                  }
                }).a
              );
            });
        },
        286: function (t) {
          t.exports = function (t) {
            return 'object' == typeof t ? null !== t : 'function' == typeof t;
          };
        },
        461: function (t) {
          t.exports = !1;
        },
        275: function (t, n, r) {
          var e = r(7),
            o = r(734),
            i = r(689),
            u = Object.defineProperty;
          n.f = r(57)
            ? Object.defineProperty
            : function (t, n, r) {
                if ((e(t), (n = i(n, !0)), e(r), o))
                  try {
                    return u(t, n, r);
                  } catch (t) {}
                if ('get' in r || 'set' in r) throw TypeError('Accessors not supported!');
                return 'value' in r && (t[n] = r.value), t;
              };
        },
        681: function (t) {
          t.exports = function (t, n) {
            return { enumerable: !(1 & t), configurable: !(2 & t), writable: !(4 & t), value: n };
          };
        },
        234: function (t, n, r) {
          var e = r(816),
            o = r(728),
            i = r(181),
            u = r(953)('src'),
            c = r(18),
            f = 'toString',
            a = ('' + c).split(f);
          (r(645).inspectSource = function (t) {
            return c.call(t);
          }),
            (t.exports = function (t, n, r, c) {
              var f = 'function' == typeof r;
              f && (i(r, 'name') || o(r, 'name', n)),
                t[n] !== r &&
                  (f && (i(r, u) || o(r, u, t[n] ? '' + t[n] : a.join(String(n)))),
                  t === e ? (t[n] = r) : c ? (t[n] ? (t[n] = r) : o(t, n, r)) : (delete t[n], o(t, n, r)));
            })(Function.prototype, f, function () {
              return ('function' == typeof this && this[u]) || c.call(this);
            });
        },
        825: function (t, n, r) {
          var e = r(645),
            o = r(816),
            i = '__core-js_shared__',
            u = o[i] || (o[i] = {});
          (t.exports = function (t, n) {
            return u[t] || (u[t] = void 0 !== n ? n : {});
          })('versions', []).push({ version: e.version, mode: r(461) ? 'pure' : 'global', copyright: '© 2020 Denis Pushkarev (zloirock.ru)' });
        },
        337: function (t, n, r) {
          var e = r(467),
            o = Math.max,
            i = Math.min;
          t.exports = function (t, n) {
            return (t = e(t)) < 0 ? o(t + n, 0) : i(t, n);
          };
        },
        467: function (t) {
          var n = Math.ceil,
            r = Math.floor;
          t.exports = function (t) {
            return isNaN((t = +t)) ? 0 : (t > 0 ? r : n)(t);
          };
        },
        875: function (t, n, r) {
          var e = r(467),
            o = Math.min;
          t.exports = function (t) {
            return t > 0 ? o(e(t), 9007199254740991) : 0;
          };
        },
        689: function (t, n, r) {
          var e = r(286);
          t.exports = function (t, n) {
            if (!e(t)) return t;
            var r, o;
            if (n && 'function' == typeof (r = t.toString) && !e((o = r.call(t)))) return o;
            if ('function' == typeof (r = t.valueOf) && !e((o = r.call(t)))) return o;
            if (!n && 'function' == typeof (r = t.toString) && !e((o = r.call(t)))) return o;
            throw TypeError("Can't convert object to primitive value");
          };
        },
        953: function (t) {
          var n = 0,
            r = Math.random();
          t.exports = function (t) {
            return 'Symbol('.concat(void 0 === t ? '' : t, ')_', (++n + r).toString(36));
          };
        },
        110: function (t, n, r) {
          'use strict';
          var e = r(985),
            o = r(639),
            i = r(32),
            u = r(337),
            c = r(875),
            f = [].slice;
          e(
            e.P +
              e.F *
                r(253)(function () {
                  o && f.call(o);
                }),
            'Array',
            {
              slice: function (t, n) {
                var r = c(this.length),
                  e = i(this);
                if (((n = void 0 === n ? r : n), 'Array' == e)) return f.call(this, t, n);
                for (var o = u(t, r), a = u(n, r), p = c(a - o), s = new Array(p), l = 0; l < p; l++)
                  s[l] = 'String' == e ? this.charAt(o + l) : this[o + l];
                return s;
              }
            }
          );
        }
      },
      n = {};
    function r(e) {
      var o = n[e];
      if (void 0 !== o) return o.exports;
      var i = (n[e] = { exports: {} });
      return t[e](i, i.exports, r), i.exports;
    }
    (r.d = function (t, n) {
      for (var e in n) r.o(n, e) && !r.o(t, e) && Object.defineProperty(t, e, { enumerable: !0, get: n[e] });
    }),
      (r.o = function (t, n) {
        return Object.prototype.hasOwnProperty.call(t, n);
      }),
      (r.r = function (t) {
        'undefined' != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, { value: 'Module' }),
          Object.defineProperty(t, '__esModule', { value: !0 });
      });
    var e = {};
    return (
      (function () {
        'use strict';
        function t() {
          return Array.prototype.slice.call(arguments).reduce(function (t, n) {
            return t + n;
          }, 0);
        }
        r.r(e),
          r.d(e, {
            default: function () {
              return t;
            }
          }),
          r(110);
      })(),
      e
    );
  })();
});
