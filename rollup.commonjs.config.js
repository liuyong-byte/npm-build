import resolve from 'rollup-plugin-node-resolve'; // 依赖引用插件
import commonjs from 'rollup-plugin-commonjs'; // commonjs模块转换插件
import babel from 'rollup-plugin-babel'; // babel 插件
// import { eslint } from 'rollup-plugin-eslint'; // eslint插件

export default {
  input: './common/index.js', // 打包的入口文件
  output: [
    {
      name: 'liu-es-utils', // 输入的包名
      file: './common_es/index.js', // 打包输出地址, 这里的导出地址就是package内main的地址
      format: 'es' // 包类型 commonjs模块，es模块，以及浏览器能识别的模块
    },
    {
      name: 'liu-cjs-utils', // 输入的包名
      file: './common_cjs/index.js', // 打包输出地址, 这里的导出地址就是package内main的地址
      format: 'cjs' // 包类型
    }
  ],
  plugins: [
    // 使用的插件
    resolve(),
    commonjs(),
    babel({
      exclude: 'node_modules/**',
      runtimeHelpers: true
    })
    // eslint({
    //   throwOnError: true,
    //   include: ['src/**'],
    //   exclude: ['node_modules/**']
    // })
  ],
  ignore: [
    'node_modules/**' // 忽略目录
  ]
};
